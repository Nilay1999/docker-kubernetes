FROM node:latest

RUN mkdir -p /server/src

WORKDIR /server/src

COPY package.json .

RUN npm install 

COPY . .

EXPOSE 8080

CMD  ["npm","start"]

